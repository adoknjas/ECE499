Analog Devices Inc.
Design Support Package
CN-0395
07/12/2017
Rev. 0

This file is designed to provide you with a brief overview of what is contained within the circuit note design support package.

**********************
*******Overview*******
**********************

CN-0395 Design Support Package contains: 
	
	Link to Circuit Note
	Link to Product Datasheets
	Link to Product Evaluation Boards and User Guides
	Schematics
	Bill of Materials
	Allegro Layout Files
	Gerber Layout Files	
	Link to Technical Support
	Link to Other Resources


**********************
***File Explanation**
**********************


Circuit Note - Copy of the circuit note this design support package was made for.  This file is provided as a PDF:

	CN0395: http://www.analog.com/CN0395

Product Datasheets - Links to all ADI component datasheets used in the circuit.  These files are provided as a PDF:

	ADN8810:	http://www.analog.com/ADN8810
	AD7988-1:	http://www.analog.com/AD7988-1
	ADG758:		http://www.analog.com/ADG758
	ADG884:		http://www.analog.com/ADG884
	ADR4540:	http://www.analog.com/ADR4540
	ADP124:		http://www.analog.com/ADP124
	AD8628:		http://www.analog.com/AD8628
	ADP196:		http://www.analog.com/ADP196

	
********************************************************
CN0395 	Evaluation Board:
	 
	http://www.analog.com/EVAL-CN0395-ARDZ


Schematic for EVAL-CN0395-ARDZ:

	EVAL-CN0395-ARDZ-Schematic-RevB.pdf
	

	
Layout files (Allegro) for EVAL-CN0395-EB1Z: 

	EVAL-CN0395-ARDZ-AllegroLayout-RevB.brd
	EVAL-CN0395-ARDZ-AllegroLayout-RevB.pdf
	

Assembly Drawings for EVAL-CN0395-ARDZ

	EVAL-CN0395-ARDZ-TopAssembly-RevB.pdf
	EVAL-CN0395-ARDZ-BottomAssembly-RevB.pdf


Gerber files for EVAL-CN0395-ARDZ:

	EVAL-CN0395-ARDZ-GRB-RevB.zip


Bill of Materials for EVAL-CN0395-ARDZ:

	EVAL-CN0395-ARDZ-BOM-RevB.cvs

User Guide for CN0395:

	http://www.analog.com/CN0395-UserGuide


Arduino Compatible Platform Board, EVAL-ADICUP360

	http://www.analog.com/EVAL-ADICUP360

	


Symbols and Footprints:

ADUCM360:
http://www.analog.com/en/design-center/packaging-quality-symbols-footprints/symbols-and-footprints/ADUCM360.html


ADN8810:
http://www.analog.com/en/design-center/packaging-quality-symbols-footprints/symbols-and-footprints/ADN8810.html

AD7988-1:
http://www.analog.com/en/design-center/packaging-quality-symbols-footprints/symbols-and-footprints/AD7988-1.html

ADG884:	
http://www.analog.com/en/design-center/packaging-quality-symbols-footprints/symbols-and-footprints/ADG884.html

ADR4540:
http://www.analog.com/en/design-center/packaging-quality-symbols-footprints/symbols-and-footprints/ADR4540.html

ADP124:
http://www.analog.com/en/design-center/packaging-quality-symbols-footprints/symbols-and-footprints/ADP124.html	
	
AD8628:	
http://www.analog.com/en/design-center/packaging-quality-symbols-footprints/symbols-and-footprints/AD8628.html
	
ADP196:
http://www.analog.com/en/design-center/packaging-quality-symbols-footprints/symbols-and-footprints/ADP196.html		
			
********************************************************


Technical Support -  If you require further technical assistance please contact us by phone, email, or our EngineerZone community.  http://www.analog.com/en/content/technical_support_page/fca.html


**********************
***Other Resources****
**********************

Resources that are not provided by Analog Devices, but could be helpful.

Gerber Viewer:  http://www.graphicode.com, http://www.viewplot.com, http://www.pentalogix.com/

Allegro Viewer: http://www.cadence.com

