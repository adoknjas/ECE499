# Airborne Laser for Telescopic Atmospheric Interference Reduction Payload
### ECE499
### Group 9
### Alexander Doknjas

# Acknowledgment

This project was made possible with the help of the project supervisor, Dr. Afzal Suleman and assistant supervisor, Dr. Justin Albert. Thanks to Brent Sirna and Paul Fedrigo for their help with getting testing equipment together. Thanks to the Chairman for funding the project and to the teaching assistants for helping to make the reports and website.

# Executive Summary

The ORCA2Sat nanosatellite will be deployed into low Earth orbit in 2021 carrying the Airborne Laser for Telescopic Atmospheric Interference Reduction (ALTAIR) Payload. This payload is based of an existing design used in a weather balloon but required extensive scope change, redefining requirements and redesigning critical systems to ensure it meets the space, power and environmental requirements of the nanosatellite and its operating environment. One component of the redesign is the change in photosensor, photosensor amplifier and photosensor signal conditioning used in the original design. The preferred photosensor was analyzed and identified as a photodiode and a literature survey of the existing solutions for photodiode amplifiers performed. The selected solution was a transimpedance amplifier operating the photodiode in photovoltaic mode. A transimpedance amplifier was then designed for the specific photodiode and analysis performed to ensure proper operation. A printed circuit board was then designed for the transimpedance amplifier circuit along with the additional circuitry to make a proof of concept prototype for the entirety of the satellite version of the ALTAIR payload. This circuit board was then underwent testing and hardware characterization, but due to the limited equipment available only rudimentary testing was performed.

# Introduction

The ALTAIR payload is a highly accurate light source that has the ability to measure its output light in real time to an accuracy 0.2%. This light source is to be placed inside a UVic Satellite Design 1U Satellite Module and integrated into the 2U ORCA2Sat nanosatellite for deployment into low Earth orbit 2021. The project has been in development for several years by Dr. Justin Albert from the UVic Physics Department and collaborators from various academic and research institutions around the world. The ALTAIR payload was originally designed for a weather balloon and did not have strict sizing requirements of a 2U nanosatellite. UVic Satellite Design is responsible for taking the ALTAIR payload and undergoing a redesign so that it will fit into the 1U satellite module. The redesign must not reinvent the wheel and only redesign the components necessary. The mechanical team has already redesigned the mechanical layout of the payload and its capabilities have been limited to ensure it will meet the sizing requirements.

The original ALTAIR payload used 4 lasers at different wavelengths (blue, green, red and near-infrared) as the light source. These laser modules had attached single-mode optical fibres that were coupled into a multi-mode fibre. This multi-mode fibre was then inserted into an integrating sphere where two Thorlabs SM05PD1B photodiodes are mounted. The integrating sphere has an injection port, an exit port and two measurement ports. The purpose of the integrating sphere is to defuse the light from the injection port evenly so that the light seen from both the measurement ports and exit port is completely uniform. This allows the observer of the integrating sphere to see the same amount of light power regardless of directionality. The photodiodes measure the injected light inside the integrating sphere. These measurements allow for the light emitted from the integrating sphere to be known to a very high accuracy (calibrated photodiodes can be as accurate as 0.2% in the 640nm wavelength) and these results can be compared to what the observer is able to measure. The results of what the observer can measure are compared to results measured from the inside the integrating sphere. Since the integrating sphere is mounted on satellite and located at approximately 350km, the difference in measurements between the payload and the observer on Earth can be accounted for by atmospheric attenuation. Thus, the atmospheric attenuation for each wavelength of measured light can be known, and all future measurements of stars can be done at a much higher degree of accuracy.

In order to account for the smaller size requirement of the 1U satellite module, the ALTAIR payload requirements have been reduced significantly from the balloon version. The main requirements that have been changed are the wavelengths of light that must be emitted, the magnitude of light that must be emitted and the preferred photosensor for this application. Due to the change in requirements, the satellite version of the ALTAIR payload electronics can simplified significantly. The main focus on the electronics simplification is the photodiode amplifier, a precision analog circuit that is specific to the photodiode in use. This circuit will need to be re-evaluated, redesigned and retested for the new photosensor and the new photosensor operating conditions.

This project will focus on the analog portion of the photosensor amplification. The photosensor amplification is interesting to design as there are many solutions, each offering their own trade-offs with no clear path to the obvious best implementation. This project was chosen because I am interested in precision analog design but have never had a project-based opportunity to learn more about it.

Successful completion of this project will lay the groundwork for future engineers and engineering students to complete the satellite version of the ALTAIR payload. In 2021 the ORCA2Sat nanosatellite will be deployed into low Earth orbit and the ALTAIR payload will begin its mission shortly afterwards. If the satellite is able to successfully complete its mission, ground based observatories will be able to measure almost any star in the night sky with an uncertainly almost a full order of magnitude better than previous. Being able to measure stars to this level of accuracy will accelerate ground based telescope projects and research into dark energy. Dark energy is currently researched by determining the acceleration of the expansion of the universe. This is done by observing the brightness of supernova as the change in brightness over time can be used to determine the speed at which that supernova is moving away from the observer. By enabling decreased uncertainties in measurements of supernova, dark energy research can be accelerated at a much faster rate then what current techniques have to offer.

# Project Goal

The goal of this project is to design the photodiode amplifier electronics of the new ALTAIR payload. The expected end product of this payload is a prototype PCB that can interface with the ORCA2Sat On-Board Computer and record measurements from the two Hamamatsu S2386 photodiodes mounted on the integrating sphere. While the end product includes the photodiode amplifier, it also includes additional circuitry that will not be discussed in detail in this report. The designed of the photodiode amplifier must be accurate enough so that the limiting factor in the accuracy of the measurement is the photodiode, not the amplifier.

# Design Objectives

While the goal of your project can be qualitative in nature, the design objectives ought to be quantitative and measurable. In this section you should highlight the technical ratings, and boundary conditions within which the design is expected to work. For example, if the project involves a filter design, the range of frequencies the filter is expected to work for has to be mentioned clearly in this section. There must be clear reasoning as to why the specified range is appropriate. 
Please note, detailed design specifications for every major subsystem of the design has to be included. For example, if the project is related to a vacuum cleaner, then details about the power supply ratings, motor ratings, power converter ratings, mechanical casing, dimension of the wheels, choice of material etc. have to be furnished with clear justification.
Before writing down the design objectives, you are expected to clearly state all the constraints within which you need to accomplish your project goal. Please recollect that for a single goal, there could be multiple objectives.
- Please write each objective statement with a quantifiable value such as performance measure or target measure. 
- Please justify each objective statement and all the constraints.  Explain why an objective was chosen.  Also explain why/how target values were chosen. Please cite relevant literature for support.

------------------------------------------------------------------------------------------------------------------

Since the actual design of the light source is not covered in this report, the design objectives focus solely on the electronics that measure the emitted light. However, some of the requirements only specify a range of allowed operating values. Because this range is open to interpretation, the design of systems not covered in this report will be briefly discussed to support the design objectives made.

- ALTAIR payload must emit light in the red wavelength that can be seen as an apparent magnitude 12 - 18 star from Earth. Therefore, the design objective is that the ALTAIR payload must be able to measure a range of light with an apparent magnitude of 12 - 18.
	- The measurement must be done with two Hamamatsu S2386 photodiodes for cross-checking the results and increased redundancy.
	- The power of light measured is maximum 24 mW at the 638 nm wavelength
- The ALTAIR payload is photometric reference for observatories, therefore the accuracy of the ALTAIR payload should be as high as possible. The limiting factor of the ALTAIR payload accuracy is the calibration of the photodiode. Currently, the National Research Council (NRC) or National Institute Standards and Technology (NIST) can calibrate the Hamamatsu S2386 photodiode to a relative expanded uncertainty as low as 0.2% for red wavelengths. Therefore, the designed electronics must not add a greater element of uncertainty that makes the photodiode not the limiting factor of uncertainty in the measurements.
- The ALTAIR payload must integrate into a UVic Satellite Design 1U Satellite Module and interface with the ORCA2Sat On-Board Computer. As this is a prototype, the actual electronics do not need to be mounted and connected into the satellite bus, but the design must account for later modifications to account for this.
	- ALTAIR payload must be able to communicate measurements over Serial Peripheral Interface with a logic level of 3.3V and no more than two chip select lines. Therefore the photosensor amplifier must output an analog voltage that is easily converted into a digital signal.
	- ALTAIR payload must be able to be powered from the ORCA2Sat Electrical Power System (EPS)
		- Photosensor amplifier must be able to be powered from one of or both of the 3.3 V or 5.5 V rails.
		- Photosensor amplifier must not consume more than 100 mW while operating
		- Photosensor amplifier must be able to be placed into a sleep or low power mode while not in operation. Standby power draw must not be more 1 mW
	- Total area of PCB should be less than 0.0025 m^2
- The ALTAIR payload is to be deployed on a nanosatellite into low Earth orbit. Therefore it must pass all NanoRacks (the launch providers) and NASA nanosatellite requirements. However, this project is a prototype of the ALTAIR payload electronics, it is not flight hardware. Therefore it is not required that it is designed to pass all NanoRacks and NASA requirements, but it must be designed so that it can pass these requirements with little to no change. The important requirements are listed below
	- Photosensor amplifier must be able to operate within a vacuum
	- Photosensor amplifier must not off-gas more than 0.1% of its total mass in a  vacuum at elevated temperatures
	- Photosensor amplifier must be able to operate in an environment of -40 to +65 degrees C
	- Photosensor amplifier must be able to withstand the NanoRacks vibration spectra
	- Photosensor amplifier must comply with NASA guidelines for hazardous materials. ALTAIR payload must be able to submit a Bill of Materials (BOM) for all components used in the design for assessment

# Literature Survey
## Introduction to Measuring Light
### Photodiode

A photodiode is a semiconductor device that converts light into electrical current. It operates as a light-controlled current source. The amount of current the photodiode produces is extremely linear to the amount of light illuminating the device and is known as the photocurrent. This makes photodiodes ideal for precision light-measuring applications as the photodiodes response has very little error over its entire range. The amount of photocurrent a photodiode generates from a given amount of incident light power is based of the photodiodes responsivity. Responsivity, also known as the photodiodes spectral response, is defined in $`A/W`$ for a given wavelength of incident light. Photodiode responsivity increases proportionally with area of active region [[7]]. However, photodiodes with large active areas are also slow to respond to changes in light incident to the active region.


![Figure 1](Electrical/Design_Documentation/Report_Images/PD_Responsivity.PNG)

**Figure 1: Typical Photodiode Responsivity Graph. Source: Hamamatsu [[8]]**

Photodiode responsivity is usually not constant. The responsivity changes depending on the wavelength of incident light. This characteristic is show in Figure 1 for a Hamamatsu S2386 photodiode and is pretty similar in shape for all silicon based photodiodes. Calibrating a photodiode through the National Institute of Standards and Technology (NIST) provides a calibration report that clearly defines the photodiodes responsivity for its entire usable range in 5nm increments. The calibration report will also state the uncertainty of each responsivity. This is important to note as designing precision light sensing instruments requires knowing exactly what the responsivity of the photodiode is.

![Figure 2](Electrical/Design_Documentation/Report_Images/PD_Model.png)

**Figure 2: Photodiode Model**

The electrical equivalent circuit for a photodiode is shown in Figure 2. It can be seen that a photodiode is essentially a light-controlled current source with a parallel junction/terminal capacitance called $`C_t`$ and a shunt resistance called $`R_s`$. The responsivity defines the amount of photocurrent generated by the current source in the equivalent circuit for a given amount of incident light. Both the terminal capacitance and shunt resistance are attributes of a photodiode that negatively effect its performance. The shunt resistance is typically very high but it will generate a noise current in the photodiode [[1]]. To ensure this noise current is as small as possible, $`R_s`$ should be as large as possible. Terminal capacitance is directly proportional to the active area of the photodiode and inversely proportional to the width of the depletion region [[1]]. The larger the active area, the slower the response time of the photodiode.

Photodiodes are very useful devices for measuring light power. They can be easily approximated with an equivalent electrical model for simulations and circuit designs. It is important to identify which photodiode and its operating specifications are to be used and what the intended application is before jumping into an electronics design.

### Photodiode Amplification

A photodiode generates a photocurrent that is proportional to the amount of light incident on its surface. The current it generates is dependant on the photodiodes responsivity. It can be determined that most photodiodes only produce a small current since the spectral response is usually in the mA/W range. The next step in accurately measuring light is to convert the current into something useful. Most digital devices such as the On-Board Computer of the ORCA2Sat nanosatellite can only interpret a logic high or logic low. This is obviously not compatible with analog current output of the photodiode. In order to convert analog current into a digital signal an analog to digital conversion must happen. 

An Analog-to-Digital Converter (ADC) takes a analog signal and converts to a digital signal. Typically an ADC will take an analog voltage signal and convert that into a digital signal, but there is another type of analog to digital conversion known as current-to-frequency converter. Since frequency can easily be measured by a digital system, current to frequency conversion is also considered an alternative. 

#### Current-to-Frequency Conversion

Current-to-frequency conversion requires a Microcontroller Unit (MCU) for measuring the output frequency. Since the payload is required to communicate to the On-Board computer using SPI communications, it must have a separate MCU in the payload module to measure the frequency, convert it into a digital signal and communicate it over the SPI interface. This is not ideal as it adds additionally complexity to the design and any sort of digital logic is a huge failure point in aerospace applications. Electronics designed for space applications must be able to withstand significant amounts of radiation since they are outside Earth's atmosphere and nanosatellites are too small to offer any sort of protection. Because these electronics are exposed to high amounts of radiation, precautions must be taken on them to ensure proper operation over the mission lifetime. Digital logic in particular is very susceptible to radiation as exposure can cause a bit flip. A bit flip is when a logic component flips its current bit to the opposite state. This can happen at random, at any time, but is significantly increased with exposure to radiation. The bit flip can happen during a instruction execute or to data stored in memory. To circumnavigate this extensive radiation hardening and error correction is required for a MCU to ensure it can load its program from memory and execute without error. A MCU is a huge failure point on a spacecraft and therefore should be avoided at all costs. The ORCA2Sat design uses a single MCU as the On-Board Computer to reduce failure points of subsystems having additional MCUs.

While the project requirements do not explicitly state that use of a MCU is not allowed, implementing an MCU capable of performing the current to frequency conversions reliably and error free requires significantly more work and complexity than alternative designs. The scope of this project is not about error free computing in demanding environments and the scope of the project would need to be expanded significantly if a MCU is to be incorporated into the design. For this reason alone, the current-to-frequency conversion alternative will not longer be discussed, regardless of its benefits over the alternatives.

#### Current-to-Voltage Conversion

Current-to-voltage conversion is significantly for precision photodiode applications and is the only other method besides current-to-frequency conversion that can offer any sort of consistency and accuracy. Current-to-voltage conversion requires a current to voltage amplifier and an ADC to convert the analog voltage output to a digital signal. The analog-to-voltage amplifier can be done with three different methods that will be discussed while the ADC will be briefly discussed as it is relevant to the analog-to-voltage amplifier design, but is not in scope for this report.

![Figure 3](Electrical/Design_Documentation/Report_Images/Transimpedance.png)

**Figure 3: Ideal Transimpedance Amplifier**

Current-to-voltage conversion is typically done with either a transimpedance amplifier, logarithmic amplifier, or differential amplifier. Transimpedance amplifier is the most popular method for current-to-voltage conversion specifically for photodiode applications. A transimpedance amplifier is commonly referred to as a current-to-voltage converter. It is an operational amplifier configured as shown in Figure 3. As input current is increased, the output voltage is increased by the formula $`V_{out} = -I_{in}*R_f`$ which is a linear relationship.

![Figure 4](Electrical/Design_Documentation/Report_Images/log.png)

**Figure 4: Ideal Logarithmic Amplifier**

Logarithmic amplifiers are very similar to transimpedance amplifiers in the sense that they can be used to convert current to voltage. An ideal logarithmic amplifier is shown in Figure 4. The difference with a logarithmic amplifier is that it uses a diode as a feedback element instead of a resistor. The resulting output voltage is defined as $`V_{out} = -V_t * ln(\frac{I_{in}}{I_s}`$ where $`V_t`$ is the diode thermal voltage and $`I_s`$ is the diode saturation current. Logarithmic amplifiers are useful because of the incredibly wide dynamic range of input currents it can amplify without large swings in output voltage. This is ideal for photodiode applications where sensing large changes in ambient light is required.

![Figure 5](Electrical/Design_Documentation/Report_Images/diff.png)

**Figure 5: Differential Amplifier For Current Sensing**

Differential amplifiers can also be used for current-to-voltage conversion. This method of current-to-voltage conversion is very popular in power systems where the current needs to be measured by converting it into a voltage without affecting the system.

A differential amplifier works by amplifying the difference in voltage between its two inputs. The output of a differential amplifier can be calculated by $`V_{out} = A_d*(V^+ - V^-)`$ where $`A_d`$ is the gain of the amplifier, $`V^+`$ is the non-inverting input voltage and $`V^-`$ is the inverting input voltage. It can be seen that placing a current sense resistor between the inverting and non-inverting inputs of a differential amplifier will create a voltage drop between the terminals when a current flows through it. This voltage drop is then amplified and the output voltage is linear with respect to current flowing through the sense resistor.

This method is not ideal of precision applications. It requires closely matched resistors on the inputs and low-drift, precision resistor for the current sensing. Additionally the design will always be error prone for low currents with respect to the maximum detectable current. This combined with the low dynamic range of the design already, the differential amplifier is not ideal for precision current-to-voltage applications.

There are many solutions for voltage-to-current conversion with the most popular methods previously outlined. Each method is ideal in a certain situation and for the sake of keeping the scope narrow, the transimpedance amplifier has been chosen. It is the industry standard for precision photodiode applications with more supporting documentation and design tools than other current-to-voltage solutions. Transimpedance amplifiers are complex devices and can be configured in many different ways. The remainder of this report will focus on on the discussion and design of the transimpedance amplifier.

## Transimpedance Amplifier Topology: Photoconductive (PC) Mode

Configuring a photodiode in photoconductive (PC) mode is when an external reverse bias is applied across the photodiode. This means that there is a voltage drop from the cathode to anode of the photodiode. This configuration increases the depletion region width of the photodiode, which in turn decreases the photodiode junction capacitance. The decrease in capacitance reduces the rise time of the photodiode which increases its response time [[1]]. This is the preferred mode for high-speed applications requiring a high bandwidth. Increasing the reverse bias amount tends to exaggerate this effect, making it worth while to reverse bias the photodiode as much as possible to get the most performance out of it. The limiting factor on the amount you can reverse bias the photodiode by is dependant on the photodiode reverse voltage rating [[2]].

The reverse bias across the photodiode has a does have a negative side effect. While it can be used to increase the speed of the diode, the reverse bias voltage also increases the leakage current through the diode, known as the dark current. The dark current is present even when the photodiode is not illuminated and therefore generates an offset error into the transimpedance amplifier [[3]]. This effect can be mitigated with various techniques, but is the major limiting factor in precision applications.

### Photoconductive Mode: Negative Reverse Bias

![Figure 6](Electrical/Design_Documentation/Report_Images/Negative_Reverse_Bias.png)

**Figure 6: Transimpedance Amplifier. $`V_{bias} < V_{ref}`$ for negative reverse bias photoconductive mode**

The circuit in Figure 6 depicts a transimpedance amplifier. This transimpedance amplifier operates the photodiode in negative reverse bias when $`V_{bias} < V_{ref}`$. Due to to characteristics of an op-amp, the device will try to ensure that its inverting and non-inverting inputs are equal. Since the non-inverting input is $`V_{ref}`$, the amplifier will change its output until $`V_{ref}`$ is present on the inverting input and there is approximately 0 voltage drop across the inputs. The photodiode will then have $`V_{ref}`$ at its cathode, creating a voltage drop from the cathode to anode.

In this configuration the output voltage can be determined by $`V_{out} = I_{PD}*R_f + V_{ref}`$, where $`R_f`$ is the feedback resistor that sets the gain of the amplifier and $`I_{PD}`$ is the photodiode current. The typical configuration for this amplifier is to set $`V_{ref}`$ to 0 V and set $`V_{bias}`$ to a negative voltage close to the maximum reverse voltage of the photodiode. In this configuration the output voltage can be determined by $`V_{out} = I_{PD}*R_f`$. Note that the output voltage increases linearly with a positive increase in photodiode current. This is the preferred configuration for high speed applications where the output is fed into additional circuitry that requires a positive rising output with respect to a positive increase in exposed light.

The main disadvantage of the negative reverse bias is the required negative $`V_{bias}`$. Integrating negative voltage supplies into designs is an additional step and in safety critical and high reliability designs, the additional circuitry is adding a new point of failure.

### Photoconductive Mode: Positive Reverse Bias

![Figure 7](Electrical/Design_Documentation/Report_Images/Positive_Reverse_Bias.png)

**Figure 7: Transimpedance Amplifier. $`V_{bias} > V_{ref}`$ for positive reverse bias photoconductive mode**

Positive reverse bias is similar to negative reverse bias in that a voltage is applied across the photodiode, however the voltage applied is positive not negative. In Figure 7, if a $`V_{bias} > V_{ref}`$ the photodiode is positive reversed bias. The output voltage is inverse polarity of the photodiode bias voltage and is determined by the $`V_{out} = V_{ref} - I_{PD}*R_f`$. This mode of operation is ideal only if the photodiode needs as large reverse bias as possible. Since large, positive supplies are easier integrate into the designs than negative supplies, this may be favorable over a negative reverse bias transimpedance amplifier.

Since the photodiode is positive reversed bias, the output will swing from $`0 V`$ to $`-V_{cc}`$. This is unfavorable for multiple reasons. Since most analog-to-digital converters measure 0 volts to a reference positive voltage, the negative output voltage of the transimpedance amplifier will need to be fed into a secondary inverting stage before fed into the analog-to-digital converter. This secondary stage is an additional active element that can either decrease noise by acting as a filter or potentially increase the noise output of the analog circuitry due to additional components acting a source of noise. Additionally, the transimpedance amplifier will require a negative supply in order to produce the negative voltage.

Not only does the positive reverse bias mode of operation require a negative voltage supply as the negative reverse bias mode, but it requires the additional secondary stage. As mentioned previously, increased complexity usually decreases reliability and is not preferred for safety critical and high-reliability designs.

## Transimpedance Amplifier Topology: Photovoltaic Mode

Photovoltaic mode is similar to negative reverse bias photoconductive mode, but there is no reverse bias voltage across the photodiode. The lack of a reverse bias across the photodiode effectively prevents any dark current from flowing through the photodiode. This significantly reduces the offset error caused by dark current and increases the linearity of the photodiode [[5]]. It is the preferred mode of operation for precision applications due to the increased linearity and theoretically no offset.

Unfortunately, no reverse bias voltage means the terminal capacitance is not decreased. This is problematic for high speed applications since terminal capacitance directly effects the bandwidth of the photodiode. Photodiodes with large active areas are favorable for a better response, but also have high terminal capacitance. Because of this characteristic, if a large active area photodiode is needed for a high speed application, photovoltaic mode is not ideal.

![Figure 8](Electrical/Design_Documentation/Report_Images/Negative_Reverse_Bias.png)

**Figure 8: Transimpedance Amplifier. $`V_{bias} = V_{ref}`$ for zero reverse bias photovoltaic mode**

The output voltage of the transimpedance amplifier with a photodiode in photovoltaic mode is $`V_{out} = I_{PD}*R_f + V_{ref}`$. Note that this is the same equation as negative reverse bias mode as the amplifier is functionally the same. The difference is the small change in how the photodiode is connected, which determines if the photodiode is operating in photovoltaic mode and photoconductive mode. Photovoltaic mode is ideal from an applications engineering perspective due to its simplicity. Unlike either of the photoconductive modes, the photovoltaic mode does not require a negative voltage supply. This greatly reduces the design requirements and simplifies the implementation significantly.

It is important to recognize that photovoltaic mode is commonly designed by tying both $`V_{bias}`$ and $`V_{ref}`$ to 0 V. However, $`V_{bias} = V_{ref}`$ where $`V_{ref}`$ equals some positive reference voltage is also an acceptable alternative. The reasoning behind this is that the typical mode, where $`V_{bias} = V_{ref}`$, forces the amplifier output to settle at 0 V if the photodiode is not illuminated. This is problematic as the amplifier will then need to be a true rail-to-rail amplifier as the output voltage needs to be the negative supply voltage. This is known as saturation and is not ideal as the amplifiers response to an input signal is will be delayed if it is saturated [[5]]. To circumnavigate this effect, it may be desirable to make $`V_{bias} = V_{ref} \neq 0 V`$. If $`V_{ref}`$ is pulled to a voltage slightly below the amplifier positive supply voltage, less then the headroom between maximum output voltage and supply voltage, the amplify is able to output $`V_{out} = V_{ref}`$ when the photodiode is not illuminated without saturating the photodiode. This may be an ideal configuration for precision applications that require a fast response from no illumination.

## Photodiode Amplifier Noise

Photodiode amplifier noise is a large concern when designing for precision applications. Any amount of noise results in uncertainty in the measurements and it needs to be reduced at all costs. Reducing the noise in a circuit can easily be brushed off until the Printed Circuit Board design stage, but in order to ensure that the design is successful, it needs to begin with the circuit design.

Noise is either caused by the photodiode itself or by the components required to make the amplifier work as intended. Noise is broken into three categories, Johnson Noise, Shot Noise and 1/f Noise.

### Johnson Noise

Johnson noise (also known as Johnson-Nyquist, thermal or Nyquist noise) is electronic noise generated by any electrical conductor with resistance. The amount of Johnson noise is dependant only on the temperature of the material and the bandwidth of the system. The amount of noise is determined by $`i_j = (\frac{4KTB}{R})^{\frac{1}{2}}`$ [[4]]. Johnson noise mainly comes from the shunt resistance of the photodiode and feedback resistor of the transimpedance amplifier. Both of these values should be noted during the design stage ensure the design is not created excessive Johnson Noise.

### Shot Noise

Shot noise is only seen in photodiodes under reverse bias [[4]]. This noise is generated in semiconductors by the rate at which charge carriers generate and recombine. Shot noise plays a significant role in overall system noise and is calculated by $`i_d = \sqrt{2eBI_d}`$, where $`i_d`$ is the current noise, $`e`$ is electron voltage, $`B`$ is detection bandwidth and $`I_d`$ is the photocurrent. It is recommended to reduce shot noise by removing minimize the DC current.

### 1/f Noise

1/f noise is not well understood and it is thought to be related to bandwidth and frequency of the electronic device. It is estimated as $`i_f = \sqrt{\frac{I_d^2B}{f}}`$ where $`i_f`$ is 1/f noise current, $`I_d`$ is photocurrent, $`B`$ is bandwidth and $`f`$ is frequency. 

### Noise in Transimpedance Amplifiers

It is important to note that the amplifier itself will add additional noise to the output of the signal. Instead of trying to analyze and identify the large noise contributors in the design, the total output noise will be observed for a selected design via simulation tools and changes made if necessary. It is important to note that shot noise can be eliminated almost entirely from the design if photovoltaic mode is used. This is due to the reverse bias voltage not being applied across the device which ensures that shot noise from the dark current is not present.

## Summary of Literature Review

The transimpedance amplifier is the obvious choice. Its mode operation has been selected to be photovoltaic mode to decrease the offset error and noise generated by the dark current. The application does not require a high speed operation of this photodiode so the alternative methods offer no additional advantages.

# Team Duties & Project Planning

This project was a multi-disciplinary project consisting of electrical engineering students and mechanical engineering students. The mechanics engineering students focused on the redesign of the mechanical components of the ALTAIR payload while the electrical students focused on the design of the photosensor amplifier. I, Alexander Doknjas am the sole electrical engineering student and performed all activities required to complete the photosensor amplifier. The challenges I encountered were time and equipment. The time required to build, test and develop this project was actually what was originally thought but I had significantly less time to complete this project due to last minute changes from the mechanical team. These changes impacted how the laser diode was to mount into the integrating sphere which changed which laser diode I was going to use. This in turn made it so my photodiode amplifier had to change to reflect the new values of light it needed to detect. Additionally, the integrating sphere was never coated with a reflective material. This prevented the validation of the integrating sphere and left no way of verifying the integrity of the photodiode amplifier with an actual photodiode attached to it. Since the photodiode is meant to amplify currents in the uA and mA range, the amplifier could not be tested and characterized fully due to lacking precision equipment capable of generating such small currents at a resolution high enough to offer valid test results. These problems were not overcome and are outlined in greater detail in the testing and validation section.

# Design Methodology & Analysis

Please detail the analysis steps for the method adopted. Analysis can be done using a set of mathematical equations, and/or using computer simulation tools. Results of the analysis backed by theoretical explanations are expected.
As a part of the report, if figures have to be included, it is preferable each graph/image/chart appears similar to the sample graph shown in Fig. 1. That is, the axes must be clearly labelled. It is preferable to have a legend. Font size should be readable in print version of the report. And a caption must be provided explaining what the image is about.

## Transimpedance Amplifier Design

The solution selected is a transimpedance amplifier operating the photodiode in photovoltaic mode.

Photodiode transimpedance amplifiers operating in photovoltaic mode produce a output voltage directly proportional to input photocurrent. The output voltage is determined by

```math
V_{out} = I_{PD}*R_f + V_{ref}
```
Where $`V_{out}`$ is output voltage, $`I_{PD}`$ is photocurrent, $`R_f`$ is the feedback resistor and $`V_{ref}`$ is the voltage applied to the non-inverting input.

The first parameter to determine in the design is the maximum output voltage of the amplifier. The design requirements state the photodiode amplifier must run off either 3.3 V or 5 V or both. In analog designs, increasing the dynamic range of the analog signal increases the Signal-to-Noise Ratio (SNR) as noise power stays relatively constant regardless of signal power. Therefore it is favorable to operate from the highest voltage possible in precision analog circuits as it will increase the SNR of the signal significantly. However, the photodiode amplifier output voltage is to be passed into an ADC to which the same logic applies.

In order to identify a maximum output voltage range, a quick look into the ADC is required. The ADC should be referenced to the highest voltage possible as the greater the full scale range, the less susceptible the ADC is to noise. The maximum voltage in the circuit is 5.5 V, while boost converters can increase the voltage to higher levels, the added complexity and noise from the switching regulator is not ideal. If the ADC reference is to be configured off 5.5 V, it is safe to say a 2.5 V or 5 V reference will be used in the design. In this situation, the ADC will be powered off 5.5 V and referenced to what is functionally 5 V. This means that if the transimpedance amplifier outputs a voltage of 5 V as its maximum output voltage, the ADC will see it as its maximum detectable value, the preferred result as the whole full-scale range of the ADC is being used. Therefore the $`V_{out MAX}`$ value of the transimpedance amplifier should be slightly less than 5 V to ensure that the transimpedance amplifier is compatible with a wide range of different ADC without clipping the signal on some ADCs that have a larger headroom. The maximum output voltage will be chosen at 4.9 V, this value can be modified later if required one the ADC is finalized.

The next parameter to determine is maximum photocurrent that should correspond to the maximum output voltage. This is determined by the maximum detectable light the photodiode will see and the responsivity of the photodiode for that wavelength of light. The target wavelength is 638 nm as per the requirements and the responsivity for the Hamamatsu S2386 photodiode is shown in Figure 1. The responsivity for a Hamamatsu S2386 at 683nm is estimated to be 0.44 A/W. Note that this is for an uncalibrated photodiode, the actual flight photodiode will be calibrated and the design might need to be changed slightly to optimize the hardware for the calibration difference. The maximum detectable light is a quite involved calculation and will not be outlined in this report and it has been set as a requirement of 24 mW. Maximum photocurrent, $`I_{PD MAX}`$ can be determined by multiplying the responsivity by the maximum detectable light power. This results in $`I_{PD MAX} = 0.44 A/W * 0.024 W = 0.01056 A`$.

Finally, it is required to determine what value of $`V_{ref}`$ will be. For the transimpedance amplifier, two different solutions will be examined. The first solution will have $`V_{ref} = V_{bias} = 0 V`$ and the second solution will look at $`V_{ref} = 0`$ and $`V_{bias} = 0.1 V`$. The first solution will have a reference voltage of 0 V and will be tested with and without a negative supply to verify if op-amp saturation is an issue. The second solution will be designed to operate the photodiode in photoconductive mode via biasing $`V_{bias}`$ with a resistor divider network from the positive supply of $`V_{DD} = 5.5 V`$ to 0.1 V. This solution is unique in the fact that the second resistor in the resistor divider network can be shorted to ground with a jumper or 0 Ohm resistor. The negative supply, $`V_{SS}`$ can then be fed a negative voltage to experiment with preventing op-amp saturation at 0 V. Due to the flexibility of the second solution, it will chosen as the preferred design. This solution can be operated in photovoltaic or photoconductive mode and will allow much more flexibility in the testing and prototyping phase.

The feedback resistor can be calculated of the first solution as follows using the parameters $`V_{out} = 4.9 V`$, $`V_{ref} = 0.1 V`$ and $`I_{PD} = 0.01056`$.

```math
R_f = \frac{V_{out} - V_{ref}}{I_{PD}}
```

The feedback resistor value is calculated to be 454.5 Ohms and the closest E96 or E192 resistor value to that is 453 Ohm. Redoing the calculation for when the circuit is operating in photovoltaic mode with $`V_{ref} = 0 V`$ results in a feedback resistor of 464 Ohm.

Several applications notes recommend adding a feedback capacitor to maintain stability. While this amplifier will be running at very slow speeds, it makes sense to design for a capacitor and not need it then end up needing one on a PCB that was not designed for it. The feedback capacitor can be determined by the following formula

```math
C_f \leq \frac{1}{2 \Pi R_f f_p}
```
Where $`C_f`$ is the feedback capacitor, $`R_f`$ is the feedback resistor and $`f_p`$ is the -3 dB bandwidth requirement of the photodiode amplifier. The required -3 dB bandwidth is not specified, but it can be assumed to be small. Feedback capacitors are usually in the picofarad range and for this application, the feedback resistor is quite small so a picofarad range capacitor results in a -3 dB bandwidth in the MHz range. For this reason a value of $`f_p = 1MHz`$ was chosen. It is noted that this capacitor value can be modified later to experiment with different values during the testing stage. Using the above formula, the value of $`C_f`$ can be determined to be less than or equal to 160 pF.

![Figure 9](Electrical/Design_Documentation/Report_Images/Trans1.png)

**Figure 9: Ideal Transimpedance Amplifier for Application Requirements**

The circuit up to now is shown in Figure 9. It is important to note that the photodiode model has been added to enable realistic simulations. The shunt resistance has not been added as the shunt resistance for the Hamamatsu S2386 photodiode is in the 10 - 100 GOhm range is not going to add anything significant to the results.

The final step before simulating the circuit is to choose a op-amp. In order to select an op-amp it is important to identify what the required gain bandwidth product (also known as unity gain) is for the circuit to operate properly. This can be determined by

```math
f_{GBW} > \frac{C_{in} + C_f}{2 \Pi R_f C_f^2}
```

where $`C_{in}`$ is the total capacitance at the inverting input, $`C_{f}`$ is the feedback capacitance and $`R_f`$ is the feedback resistor. The total capacitance at the inverting input is the sum of all the capacitances to ground from the inverting input. This can be summarized as $`C_{in} = C_t + C_D + C_{CM}`$ where $`C_t`$ is the photodiode terminal capacitance, $`C_D`$ is the differential capacitance of the op-amp and $`C_{CM}`$ is the common mode capacitance. The photodiode terminal capacitance is in the nF range whole differential and common mode capacitance are usually 0.1 - 10 pF for good op-amps. Since these capacitance are so small compared to the terminal capacitance, they can be ignored [[6]] and the previous formula rewritten as

```math
f_{GBW} > \frac{C_{t} + C_f}{2 \Pi R_f C_f^2}
```

This formula can be used to determine the gain-bandwidth product and is required before selecting an op-amp. Using $`C_t = 4.3 nF`$, $`C_f = 160 pF`$, and $`R_f = 464 \Omega`$, the value of $`f_{GBW} = 60 MHz`$ was calculated.

## Operational Amplifier Selection Overview

Before an op-amp can be selected, the requirements of the amplifier must first be identified. The hard requirements are outlined quickly as follows: op-amp must be able to run off a 5 V - 5.5 v supply and must have a gain-bandwidth product (GBW) of 22 MHz or greater. Note that the GBW is not a strict requirement as its based of the bandwidth of the circuit which has been arbitrarily chosen as 1 MHz. If specification poses difficult to meet, the bandwidth can be narrowed.

These two requirements are not enough alone to select an op-amp. Texas instruments has over 1000 different models that meet this requirement. Further specifications are outlined.

- GBW should be larger than 22 MHz, larger is better to ensure stability
- Operating voltage must be between 5.1 V and 5.5 V. This is to ensure headroom between the maximum output voltage of 4.9 V and the maximum supply voltage of 5.5 V
- Rail-to-rail output is ideal. This enables the op-amp to output a voltage very close to the supply voltage. In the this application case the output is trying to settle to 0.1 V in photoconductive mode and 0 V in photovoltaic mode. Since negative supplies are undesirable, it is advantageous to use an op-amp with rail-to-rail output to ensure the output is as close to 0V when the photodiode sees no light and is operating in photovoltaic mode.
- Input offset voltage and input offset voltage drift should be as low as possible to reduce offset error in calculations. It is more important to ensure that the drift is low than the offset itself as the offset can be corrected for in software.
- Input bias current is critical that it remains as low as possible. Any current flowing into the op-amp will result in error when measuring the photocurrent. This input bias current needs to be several orders of magnitude less than the photocurrent. Luckily most precision op-amps offer input bias currents of less than 10 pA
- Noise is also another factor to pay attention to. The op-amp itself will introduce noise to the circuit and its important to make sure the op-amp is low noise. Most op-amps specify input voltage and current noise density. For photodiode applications its more important to ensure the op-amp has low current noise. This value should be as low as possible, but any modern op-amp will usually have noise density in the fA range so its not as crucial to ensure this requirement is met as any typical op-amp will meet that requirement.
- Operating power consumption is important to note. Power consumption should be as low as possible to prevent excess heat dissipation and power draw/
- Package type is another consideration. It is important that the package have leads to ensure hand soldering or direct pin-to-pin soldering of the op-amp and photodiode. It is also important to note the temperature range of the package. The required temperature range is -40 to +65 degrees C and most op-amps meet this specification, but its important to double check before committing to a design.
- Component grade is another consideration. For nanosatellite applications automotive, high-reliability-military and space grade components should be used whenever possible.
- Price is another consideration. It is important to not specify a op-amp that is too expensive to buy in bulk so that it hurts the speed of the prototyping and testing stage. Plan on buying extras and more than enough to populate several PCBs for group testing and debugging
- Lastly, availability is important. Ensure the op-amp is readily available and stocked on large suppliers such as Digi-Key and Mouser.

With these op-amp requirements in mind, the search for a precision, low noise, rail-to-rail output amplifier can begin. Texas Instruments offers three models of op-amp that should work well for this applications. The op-amps are the OPA300/301, OPA320 and OPA365. Lets compare the main differences between these amplifiers.

| Specification | OPA300/301 | OPA320 | OPA365 |
|--------------------------------|-------------|-------------|-------------|
| Voltage Supply: $`V_S`$        | $`2.7 - 5.5 V`$ | $`1.8 - 5.5 V`$ | $`2.2 - 5.5 V`$ |
| Gain Bandwidth Product: $`GBW`$| $`150 MHz`$ | $`20 MHz`$ | $`50 MHz`$ |
| Rail-to-rail Output            | $`(V+) - 0.1 V`$ to $`(V-) + 0.1 V`$ | $`(V+) - 0.01 V`$ to $`(V-) + 0.01 V`$ | $`(V+) - 0.01 V`$ to $`(V-) + 0.01 V`$ |
| Input Offset Voltage: $`V_{OS}`$| $`1 mV`$ | $`40 uV`$ | $`100 uV`$ |
| Input Bias Current: $`I_{B}`$ | $`+/- 5 pA`$ | $`+/- 0.2 pA`$ | $`+/- 0.2 pA`$ |
| Input Current Noise Density, f < 1 kHz: $`i_e`$ | $`1.5 fA/\sqrt{Hz}`$ | $`0.6 fA/\sqrt{Hz}`$ | $`> 4 fA/\sqrt{Hz}`$ |
| Operating Current Consumption: $`I_Q`$ | $`13 mA`$ | $`1.6 mA`$ | $`4.6 mA`$ |
| Available in package with leads? | yes | yes | yes |
| Highest component grade available | Consumer | Automotive | HiRel Enhanced |
| Unit Cost (CAD) | $3.45 | $3.20 Consumer, $3.77 Automotive | $2.6 Consumer, $9.68 HiRel Enhanced |

It can be seen from the table above that the OPA300/301 is worse in almost everyway except for the GBW. Since this is a low speed precision application, either the OPA320 or OPA365 are a better choices. The OPA320 and OPA365 are actually pin-compatible so the PCB can be designed for both and both be purchased and assembled if extensive testing is required. It is important to note that with the loose requirement of GBW greater than 60 MHz, neither op-amp is able to meet this specification. Moving to high-speed op-amps increases the offset voltage and input bias current and should be avoided. Instead the scope of the bandwidth will be modified. The -3 dB bandwidth will be decreased from $`f_p = 1 MHz`$ to $`f_p = 10 kHz`$. Re-doing the calculations for $`C_f`$ changes the value to $`C_f = 16 nF`$ it is important to note that 16 nF capacitors are not that common and it would be better to chose a 15 nF capacitor in ensure availability of the part. $`f_{GBW}`$ can be determined to be a much more realistic $`f_{GBW} \approx 30 kHz`$. 

## Transimpedance Amplifier Preliminary Design Summary

To summarize the component values for a transimpedance amplifier capable of operating in both photovoltaic and photoconductive modes with a maximum output voltage of 4.9 V, -3dB bandwidth of 10 kHz and designed to amplify a Hamamatsu S2386 photodiode, the table below has been provided. The selected amplifier is the OPA320 due to its smaller offset voltage and input current noise density.

| Mode | $`R_f`$ | $`C_f`$ | $`f_{GBW}`$ |
|------|---------|---------|-------------|
| Photovoltaic, $`V_{ref} = 0 V`$ | $`464 \Omega`$ | $`15 nF`$ | $`30 kHz`$ |
| Photoconductive, $`V_{ref} = 0.1 V`$ | $`453 \Omega`$ | $`15 nF`$ | $`30 kHz`$ |

## Design Analysis

The first step of the analysis is to verify the DC transfer characteristic of the amplifier. This is to ensure that with a linear increase in photocurrent, the amplifier linearly increases the output voltage. The circuit was simulated using a OPA365 SPICE model provided by Texas Instruments and ideal passive components in the TINA-TI simulation program. The initial circuit can be seen in Figure 10.

![Figure 9](Electrical/Design_Documentation/Report_Images/Initial_Schematic.png)

**Figure 9: Initial schematic for Transimpedance Amplifier Analysis**

![Figure 10](Electrical/Design_Documentation/Report_Images/Initial_DC.png)

**Figure 10: DC Transfer Characteristic**

The initial results are promising. The transimpedance amplifier creates an extremely linear output voltage from 0.1 V to 4.8 V with a gain of 464 V/A. Increasing the voltage supply to 5.1 V enables the op-amp to output the ideal 4.9 V and should not cause any problems as the supply rail is 5.5 V. In order to output a true 0 V, a negative supply must be used on the $`VSS`$ instead of just connecting it to ground. This is not ideal, but it is a solution to the problem.

Adding a negative supply to the $`V_{SS}`$ rail and increase the supply voltage to 5.1 V creates the ideal results as shown in Figure 11.

![Figure 11](Electrical/Design_Documentation/Report_Images/Improved_Schematic.png)

**Figure 11: Improved Transimpedance Amplifier**

![Figure 12](Electrical/Design_Documentation/Report_Images/Improved_DC.png)

**Figure 12: Improved DC Transfer Characteristic**

The circuit preforms much closer to the ideal scenario with the addition of the negative supply. The negative supply will also prevent the op-amp from saturating at 0 V and is looking more and more like it will be needed after all. 

![Figure 13](Electrical/Design_Documentation/Report_Images/Improved_AC.png)

**Figure 13: Improved AC Transfer Characteristic**

The AC transfer characteristic is another way to determine the if the photodiode amplifier circuit will behave as intended. The AC transfer characteristic is shown in Figure 13 for the improved transimpedance amplifier circuit. The result really quite good, the -3 dB bandwidth is measured to be at 22 kHz which exceeds the expectations and requirements.

![Figure 14](Electrical/Design_Documentation/Report_Images/Improved_Stability_Schematic.png)

**Figure 14: Improved Transimpedance Amplifier Stability Analysis Simulation**

![Figure 15](Electrical/Design_Documentation/Report_Images/Improved_Stability.png)

**Figure 15: Improved Transimpedance Amplifier Stability Results**

Finally a stability analysis is recommend for this design [[6]]. To do this and inductor is place in the feedback path to allow the circuit to settle to a DC bias point, but to act as an open circuit when it sees AC. The resulting simulation is used to determine stability by measuring open loop gain. The phase margin can be seen at 0 dB to be 85 degrees. This is not a high speed circuit so that value is perfectly acceptable.

![Figure 16](Electrical/Design_Documentation/Report_Images/Improved_Noise_Output.png)

**Figure 16: Improved Transimpedance Amplifier Noise Output

Lastly, the noise perform of the circuit should be simulated to verify that there are no major issues. This is done by running a output noise analysis on the circuit in Figure 11. The results can be seen in Figure 16 and show that there is no significant source of noise. It is important to note that this is theoretical noise. The PCB layout itself will be the major contributor of noise and ensuring a proper layout is important.

The results are quite promising. Usually photodiode transimpedance amplifiers required feedback resistors in the MOhm range in order to the required gain. Luckily the Hamamatsu S2386 has exceptional responsivity and produces several orders of magnitude more photocurrent for the same exposure to light than other photodiodes. This is ideal as the larger the feedback resistor, the larger the Johnson noise is seen from that component. Typical solutions will often need to add a T-network and a secondary stage to get the required gain. This solution is fortunate enough to not require any additional circuitry as the gain can be completely easily within one stage. While not shown here, the circuits were re-simulated with a $`V_{ref} = 0.1 V`$ and $`V_{SS} = 0 V`$ to double check that it would be worth while to test this circuit in photoconductive mode if desired. The results are satisfactory and the circuit can be configured as a photoconductive mode transimpedance amplifier. It is also worth mentioning that the Hamamatsu S2386 photodiode is a windowless photodiode and needs to be assembled and operating in a ISO class 5 clean room or better. This is obviously not ideal for early prototyping so the original Thorlabs SM05PD1B was used. The only major difference is the decrease in responsivity and junction capacitance. Re-running the calculations and simulations shows that using a $`R_f = 604 \Omega`$ corrects for the change in responsivity and the smaller terminal capacitance has very little negative effect on the circuit.

# Final Design Details

The final design of the photodiode amplifier consists of a single stage transimpedance amplifier. The amplifier and required circuitry is to be supplied from a 5.1 V rail by a liner regulator powered off the 5.5 V rail. The linear regulator was selected due to its high Power Supply Rejection Ration (PSRR) and low drop-out voltage to enable ultra-low noise voltage supply of the op-amp. A negative voltage supply was not used in this design as part of the testing process is to verify if photovoltaic mode with a negative $`V_{SS}`$ supply is required, or if operating in photoconductive mode is good enough. The PCB designed for testing includes a huge amount of additional circuitry not covered in this report. This include a ADC with voltage reference and a programmable constant current driver. The intentions behind this additional circuitry is so the testing and verification can happen with the use of an integrating sphere and an actual laser diode illuminating the sphere and photodiode. Since there are many issues that can exposed with the amplifier outside of feeding it a constant current as per the simulation, it is a good idea to connect the actual photodiode to it and observe the real-world characteristic of the circuit. The complete design is detailed below in Figure 17 through 19.

![Figure 17](Electrical/Design_Documentation/Report_Images/Schematic_Top.PNG)

**Figure 17: PCB Top Schematic. Note that detailed schematics can be found on the project website**

![Figure 18](Electrical/Design_Documentation/Report_Images/PCB1.PNG)

**Figure 18: Render of PCB**

![Figure 19](Electrical/Design_Documentation/Report_Images/PCB2.PNG)

**Figure 19: Overview of PCB Layout**

The objectives will be outlined here and determined if they the design passes the requirements.

- The measurement must be done with two Hamamatsu S2386 photodiodes for cross-checking the results and increased redundancy.
	- Completed as PCB contains duel transimpedance amplifiers and a synchronous sampling dual channel ADC.
- The power of light measured is maximum 24 mW at the 638 nm wavelength
	- Completed by design of the transimpedance amplifier for a Hamamatsu S2386 photodiode with a responsivity of 0.44 A/W. The maximum detectable light is 24 mW and minimum detectable light is 0.5 mW. This is done by determine the feedback resistor value of amplifier and verified by the DC transfer characteristic.
- The ALTAIR payload is photometric reference for observatories, therefore the accuracy of the ALTAIR payload should be as high as possible. The limiting factor of the ALTAIR payload accuracy is the calibration of the photodiode. Currently, the National Research Council (NRC) or National Institute Standards and Technology (NIST) can calibrate the Hamamatsu S2386 photodiode to a relative expanded uncertainty as low as 0.2% for red wavelengths. Therefore, the designed electronics must not add a greater element of uncertainty that makes the photodiode not the limiting factor of uncertainty in the measurements.
	- The transimpedance amplifier is designed and verified to be as linear and as low drift as possible. The transimpedance amplifier is not designed to hardware calibrated, this is failure point in high-reliability designs. This circuit will have to be tested and verified to determine an out-of-the-box accuracy. Once that is determined, the circuit can be characterized and calibrations added in software. Finally, the circuit will have to be sent off to a 3rd party with proper equipment to verify the integrity of the design. It is safe to say that this objective was not met, but the design is not prohibiting this objective from being met in the future.
- ALTAIR payload must be able to communicate measurements over Serial Peripheral Interface with a logic level of 3.3V and no more than two chip select lines. Therefore the photosensor amplifier must output an analog voltage that is easily converted into a digital signal.
	- The chosen ADC uses SPI to communicate. The chosen current driver uses SPI to communication. This objective has been met even if it is out of scope of this report.
- Photosensor amplifier must be able to be powered from one of or both of the 3.3 V or 5.5 V rails.
	- Photodiode amplifier is designed to run off $`V_{DD} = 5.1 V`$ and $`V_{SS} < -2 V`$. The $`V_{DD}`$ rail is powered by a low noise linear regulator that steps down and filters the 5.5 V rail. The negative supply is not included in this design so this requirement is not met. This will have to be assessed in future designs as the negative rail really improves the performance significantly but it can be difficult to implement.
- Photosensor amplifier must not consume more than 100 mW while operating
	- Transimpedance amplifier consumes a absolute maximum current draw of 1.85 mA when running off a 5.5 V rail. This means the op-amp will never consume more than 10 mW of power.
- Photosensor amplifier must be able to be placed into a sleep or low power mode while not in operation. Standby power draw must not be more 1 mW
	- The photodiode amplifier has a shutdown pin that can be used or the 5.1 V regulator can be disabled. The worst case scenario is just amplifier can be shut down, in which case it consumes 0.5 uA maximum. Running of a 5.5 V rail, this equates to a shutdown power consumption of 3 uW, well within the objectives.
- Total area of PCB should be less than 0.0025 m^2
	- Total PCB area is 0.01 m^2. It does not meet the size requirements of the flight PCB. This objective was not met, but that is fine as its just a prototype.
- Photosensor amplifier must be able to operate within a vacuum
	- It is assumed all electronics operate fine within a vacuum. There is no special chemistry or rare sensing elements. The Hamamatsu S2386 photodiode will be used without the window so the outgassing of the epoxy holding the window in place will not cause problems. The flight hardware will have to be baked out in an oven and then tested and verified operational in a thermal vacuum test before it can be passed as flight hardware. As of now this objective has been met.
- Photosensor amplifier must not off-gas more than 0.1% of its total mass in a  vacuum at elevated temperatures
	- To be determined, it should not but this is difficult to say definitively. This objective has not been met.
- Photosensor amplifier must be able to operate in an environment of -40 to +65 degrees C
	- The op-amp operating range is -40 to +125 degrees C. All passives are rated for the same or better temperature range. This objective has been met.
- Photosensor amplifier must be able to withstand the NanoRacks vibration spectra
	- Mechanical design issue, to be verified later by the mechanical team. Currently it is safe to say electronics survive the vibration spectra without issues, especially since all connectors are automotive grade. This object has not been met.
- Photosensor amplifier must comply with NASA guidelines for hazardous materials. ALTAIR payload must be able to submit a Bill of Materials (BOM) for all components used in the design for assessment
	- Bill of materials has been created and meets the outlined guidelines. It will still need to be verified by NASA so currently this objective has not been met.

# Testing & Validation
## Test Plan

The goal of this amplifier is convert photodiode photocurrent into output voltage within a range that can be measured by an ADC. It must ensure that it does not add any error so great that the photodiodes calibration uncertainty is non longer the limiting factor in the accuracy of the measurement. The testing plan to verify the overall integrity of the design is as follows

- Preliminary Testing: Verify the basic operation of the photodiode amplifier
- Functional Testing/Integration Testing: Verify the more advanced characteristics of the photodiode amplifier
- Characterization testing: characterize the photodiode amplifier so any non-linearity or offset can be calibrated for in software. This is then used to do the final performance testing
- Performance Testing: Extensive testing and validation to validate long term stability and reliability over a range of operating conditions while maintaining the required accuracy

### Preliminary Testing

Preliminary testing consists of assembling the PCB and verify that the linear regulator is output a stable 5.1 V. It is essential to check the PCB for any short circuits or solder joints left open. The transimpedance amplifier needs to pass preliminary testing by showing that it can output 0 V when there no photocurrent input and that it outputs 4.9 V when there is maximum photocurrent.

1. Assemble linear regulator and transimpedance amplifier
2. Use a multiple meter to probe the 5.5 V and 5.1 V to ground to check for shorts
3. Power the circuit via the 5.5 V voltage input from a benchtop power supply. Verify the voltage regulator is outputting a stable 5.1 V
4. Remove the 5.5 V supply and power the transimpedance amplifier by a benchtop supply set to 5.1 V. Use a multimeter to probe the output of the transimpedance amplifier to ensure that it is outputting the minimum voltage corresponding to 0 photocurrent.
5. Connect a microamp/milliamp current source to the non-inverting input of the transimpedance amplifier. Starting with 0 mA of current, increase current in 0.1 mA increments until the maximum photocurrent threshold is reached at approximately 11 mA. Ensure to go past this value by a few points to ensure the output begins to clip. Take these points and plot them on a graph and verify that the DC transfer characteristic is from 0 V to 4.9 V for a photocurrents of 0 to 11 mA. Use the graph to check the linearity of the photodiode amplifier.

### Functional Testing

Functional testing consists testing the linear regulator and transimpedance amplifier together and ensuring their operation meets the requirements. It is also time to check the noise and frequency response of the system.

1. Enable the linear regulator and ensure the transimpedance amplifier is powered off the linear regulator
2. Open circuit the photodiode connection from the non-inverting input of the transimpedance amplifier. Use an oscilloscope to probe the output voltage to examine the noise floor of the transimpedance amplifier
3. Connect the photodiode and perform step 5 of the preliminary testing while still measuring the output voltage noise with oscilloscope.
4. Leaving the oscilloscope connected, feed the transimpedance amplifier 1 - 22 kHz current pulses to determine the -3 dB bandwidth.
5. Connect a precision light source and a calibrated photodiode into an known-good integrating sphere. Using a known-good precisions light measuring instrument, compare the results between the transimpedance amplifier and the calibrated photodiode with the reference instrument. Observe any differences in the measurements.

Characterization testing and performance testing will not be discussed as they are out of scope of just the transimpedance amplifier design.

### Testing Results

This project required extensive testing and test equipment to verify the integrity and accuracy of the design. Due to lacking access to such equipment in the UVic engineering department and not allowing enough time to secure access to this equipment outside of the UVic engineering department, only steps 1 through 4 of the preliminary testing was completed. Step 5 was attempted to be completed but the current source was only accurate t a few milliamps of current and the resulting graph is shown below in Figure 18.

![Figure 21](Electrical/Design_Documentation/Report_Images/Test_Data.png)

**Figure 21: Inconclusive Test Data**

Using this graph it can be determined that the standard deviation of the slope of the graph measured at various parts is 2.4%. It is also noted that the slope of the graph is steeper than expected. This is most likely due to some offset, but there was only 5 points about to be measure on the graph and none of them were able to characterise the output of the amplifier close to the minimum and maximum values. Further testing with better equipment is required, these results are net very useful.

# Discussion and Recommendations

Due to the disappointing test results, no conclusion can be drawn about the prototype other than the PCB design includes no shorts, linear regulator acts as a linear regulator and the transimpedance amplifier does output approximately the correct response for a specific photocurrent. The testing and verification section was not well understood until the project was nearly completed and it was discovered that specialized equipment was needed to perform even the most rudimentary testing. This is unfortunate as the test results will need to be gathered eventually, but until then the assembled PCB sits in an anti-static bag waiting.

Recommendations to the project can be made outside the testing results. While this design did not include a negative supply, it relies on one to operate to its full potential. This negative supply was later discovered to be quite troublesome to implement as it introduces a large source of noise. A good recommendation would be build and test both the photovoltaic mode photodiode amplifier and the photoconductive mode amplifier and see what the difference in noise is. The photodiode in photoconductive mode is only reverse biased with 0.1 V, which equates to about 10 pA of dark current. Compared to the 10 mA full scale current this might be insignificant enough for the photoconductive mode amplifier to be more low noise than a photovoltaic mode amplifier with a negative supply. Additionally there are some more complex designs that do not require a negative supply that were not discussed in the report. It would be highly advantageous to do a serious analysis on the use of designs with a negative supply and the feasibility of implementing it successfully.

# Conclusion

The design accomplishes all objectives within reason but was unable to verify the design due to lacking the test equipment capable of providing precision current. The photodiode amplifier designed theoretically works very well but the conclusion is inclusive due to lacking real-world test results. Additionally, the chosen design relies on converting a 5.5 V rail into a low noise, negative supply rail to bias the negative voltage supply of the op-amp. This was thought to be simple and left out of the prototype due to time constraints. However it has become apparent that this might not be so simple and the most obvious solution is no longer so obvious due to this limitation. Luckily the prototype was designed in such away that the transimpedance amplifier could be configured to work with multiple different photodiodes running to either photoconductive or photovoltaic mode. This enables the prototype to provide some very useful feedback into other designs besides the one that was chosen, specifically designs that do not required the potentially limiting negative voltage rail.

# References

[1] UDT Sensors, "Photodiode Characteristics and Applications," UDT Sensors Inc, [Online]. Available: http://www.phas.ubc.ca/~beaudoin/PDS%20Papers/Position%20Detector%20Info/UDT_PhotodiodeCharacteristics.pdf. [Accessed August 2018].

[2] B. Black, "Transimpedance Amplifiers for Wide Range Photodiodes Have," Linear Technology, [Online]. Available: http://www.analog.com/media/en/technical-documentation/technical-articles/S54_EN-Circuits.pdf. [Accessed August 2018].

[3] Burr-Brown, "DESIGNING PHOTODIODE AMPLIFIER CIRCUITS WITH OPA128," Burr-Brown Corporation, January 1994. [Online]. Available: http://www.ti.com/lit/an/sboa061/sboa061.pdf. [Accessed August 2018].

[4] https://depts.washington.edu/mictech/optics/me557/detector.pdf, "Engineering Optics," [Online]. Available: https://depts.washington.edu/mictech/optics/me557/detector.pdf. [Accessed August 2018].

[5] S. W. C. K. Walt Kester, "HIGH IMPEDANCE SENSORS," Analog Devices, [Online]. Available: http://www.analog.com/media/en/training-seminars/design-handbooks/sensor-signal-cond-sect5.PDF. [Accessed August 2018].

[6] J. Caldwell, "1 MHz, Single-Supply, Photodiode Amplifier Reference," TI Designs, November 2014. [Online]. Available: http://www.ti.com/lit/ug/tidu535/tidu535.pdf. [Accessed August 2018].

[7] Learningaboutelectronics.com, "What is a Photodiode?," Learningaboutelectronics.com, [Online]. Available: http://www.learningaboutelectronics.com/Articles/What-is-a-photodiode. [Accessed August 2018].

[8] Hamamatsu, "S2386-8K," Hamamatsu, August 2017. [Online]. Available: https://www.hamamatsu.com/resources/pdf/ssd/s2386_series_kspd1035e.pdf. [Accessed August 2018].

[9] Texas Instruments, "50-MHz Low-Distortion High-CMRR Rail-to-Rail I/O, Single-Supply Operational Amplifier," Texas Instruments, August 2011. [Online]. Available: http://www.ti.com/lit/ds/symlink/opa365-ep.pdf. [Accessed August 2018].

[10] Texas Instruments, "50-MHz Low-Distortion High-CMRR Rail-to-Rail I/O, Single-Supply Operational Amplifier," Texas Instruments, August 2011. [Online]. Available: http://www.ti.com/lit/ds/symlink/opa365-ep.pdf. [Accessed August 2018].

[11] Burr-Brown, "Low-Noise, High-Speed, 16-Bit Accurate, CMOS," Texas Instruments, June 2007. [Online]. Available: http://www.ti.com/lit/ds/sbos271d/sbos271d.pdf. [Accessed August 2018].

[1]: http://www.phas.ubc.ca/~beaudoin/PDS%20Papers/Position%20Detector%20Info/UDT_PhotodiodeCharacteristics.pdf
[2]: http://www.analog.com/media/en/technical-documentation/technical-articles/S54_EN-Circuits.pdf
[3]: http://www.ti.com/lit/an/sboa061/sboa061.pdf
[4]: https://depts.washington.edu/mictech/optics/me557/detector.pdf
[5]: http://www.analog.com/media/en/training-seminars/design-handbooks/sensor-signal-cond-sect5.PDF
[6]: http://www.ti.com/lit/ug/tidu535/tidu535.pdf
[7]: http://www.learningaboutelectronics.com/Articles/What-is-a-photodiode
[8]: https://www.hamamatsu.com/us/en/product/type/S2386-8K/index.html
[9]: http://www.ti.com/lit/ds/symlink/opa365-ep.pdf
[10]: http://www.ti.com/lit/ds/symlink/opa365-ep.pdf
[11]: http://www.ti.com/lit/ds/sbos271d/sbos271d.pdf